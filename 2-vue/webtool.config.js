const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = {
	appEntry: './src/entry.js',
	define: {
		__VUE_OPTIONS_API__: true,
		__VUE_PROD_DEVTOOLS__: false
	},
	configureEncore(encore) {
		encore.enableVueLoader(() => {}, {
			runtimeCompilerBuild: false,
			version: 3
		});
	},
	configureWebpack(webpack) {
		webpack.plugins.push(new FaviconsWebpackPlugin('./src/assets/logo.png'));
	},
	configureXO(xo) {
		xo.extends.push('plugin:vue/vue3-recommended');
	}
};
